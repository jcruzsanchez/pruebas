package clases;

import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		GestorContabilidad gestor = new GestorContabilidad();
		Factura factura = new Factura("sss", LocalDate.now(), "Factura 1", 2, 1);
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 2, 1);
		gestor.crearFactura(factura);
		gestor.crearFactura(factura1);
		System.out.println(gestor.getListaFacturas().size());

	}

}
