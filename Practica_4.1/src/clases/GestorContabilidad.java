package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		super();
		this.listaFacturas = new ArrayList<>();
		this.listaClientes = new ArrayList<>();
	}
	
	public Cliente buscarCliente(String dni) {
		for(Cliente item : listaClientes) {
			if(item.getDni().equals(dni)) {
				return item;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo) {
		for(Factura item : listaFacturas) {
			if(item.getCodigoFactura().equals(codigo)) {
				return item;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente cliente) {
		if(!listaClientes.contains(cliente)) {
			listaClientes.add(cliente);
		}		
	}
	
	public void crearFactura(Factura factura) {
		if(this.buscarFactura(factura.getCodigoFactura()) == null) {
			listaFacturas.add(factura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		if(listaClientes.size() > 0) {
			Cliente cliente = listaClientes.get(0);
			for(Cliente item : listaClientes) {
				if(item.getFechaAlta().compareTo(cliente.getFechaAlta()) > 0) {
					cliente = item;
				}
			}
			return cliente;
		}else {
			return null;
		}
	}
	
	public Factura facturaMasCara() {
		if(listaFacturas.size() > 0) {
			Factura factura = listaFacturas.get(0);
			for(Factura item : listaFacturas) {
				if(item.calcularPrecioTotal() > factura.calcularPrecioTotal()) {
					factura = item;
				}
			}
			return factura;
		}else {
			return null;
		}
	}
	
	public float calcularFacturacionAnual(int anno) {
		float resultado = 0;
		for(Factura factura : listaFacturas) {
			if(factura.getFecha().getYear() == anno) {
				resultado += factura.calcularPrecioTotal();
			}
		}
		return resultado;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		this.buscarFactura(codigoFactura).setCliente(this.buscarCliente(dni));
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int resultado = 0;
		for(Factura factura : listaFacturas) {
			if(factura.getCliente() != null) {
				if(factura.getCliente().getDni().equals(dni)) {
					resultado++;
				}
			}
		}
		return resultado;
	}
	
	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}
	
	public void eliminarCliente(String dni) {
		if(cantidadFacturasPorCliente(dni) == 0) {
			listaClientes.remove(buscarCliente(dni));
		}		
	}
	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	
	
}
