package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestFacturas {

	GestorContabilidad gestor;
	Factura factura;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		gestor = new GestorContabilidad();
		factura = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	//gestor devuelve la factura buscada que la tiene guardada
	@Test
	void testBuscarFacturaQueSiExiste() {
		gestor.getListaFacturas().add(factura);
		Factura esperado = factura;
		Factura actual = gestor.buscarFactura("sss");
		assertEquals(esperado, actual);
	}
	//gestor devuelve null si no encuentra la factura
	@Test
	void testBuscarFacturaQueNoExiste() {
		gestor.getListaFacturas().add(factura);
		Factura esperado = null;
		Factura actual = gestor.buscarFactura("no existe");
		assertEquals(esperado, actual);
	}
	//gestor no crea la factura si ya la tiene contenida
	@Test
	void testCrarFacturaQueSiExiste() {
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		gestor.crearFactura(factura);
		gestor.crearFactura(factura1);
		assertFalse(gestor.getListaFacturas().contains(factura1));
	}
	//el gestor almacena la factura que es nueva
	@Test
	void testCrarFacturaQueNoExiste() {
		gestor.crearFactura(factura);
		assertTrue(gestor.getListaFacturas().contains(factura));
	}
	//el gestor devuelve la factura mas cara
	@Test
	void testFacturaMasCaraArrayContieneFacturas() {
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 6, 6);
		gestor.getListaFacturas().add(factura);
		gestor.getListaFacturas().add(factura1);
		Factura esperada = factura1;
		Factura actual = gestor.facturaMasCara();
		assertEquals(esperada, actual);
	}
	//el gestor devuelve null si no tiene facturas almacenadas
	@Test
	void testFacturaMasCaraArrayVacio() {
		Factura esperada = null;
		Factura actual = gestor.facturaMasCara();
		assertEquals(esperada, actual);
	}
	//calcula la suma de los precios totales de facturas para un a�o dado
	@Test
	void testCalcularFacturacionAnualArraySiContieneFacturasDeEseAnno() {
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura2 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura3 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		gestor.getListaFacturas().add(factura);
		gestor.getListaFacturas().add(factura1);
		gestor.getListaFacturas().add(factura2);
		gestor.getListaFacturas().add(factura3);
		float esperada = 100;
		float actual = gestor.calcularFacturacionAnual(2018);
		assertEquals(esperada, actual);
	}
	//gestor debe devolver 0 si no existen facturas para un a�o dado
	@Test
	void testCalcularFacturacionAnualArrayNoContieneFacturasDeEseAnno() {
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura2 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura3 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		gestor.getListaFacturas().add(factura);
		gestor.getListaFacturas().add(factura1);
		gestor.getListaFacturas().add(factura2);
		gestor.getListaFacturas().add(factura3);
		float esperada = 0;
		float actual = gestor.calcularFacturacionAnual(1985);
		assertEquals(esperada, actual);
	}
	//gestor elimina una factura
	@Test
	void testEliminarFacturaQueSiExiste() {
		gestor.getListaFacturas().add(factura);
		gestor.eliminarFactura(factura.getCodigoFactura());
		assertFalse(gestor.getListaFacturas().contains(factura));
	}
	//gestor asigna un cliente a una factura cuando los dos existen
	@Test
	void testAsignarClienteAFactura_FacturaExiste_ClienteExiste() {
		Cliente cliente = new Cliente("pepe", "12346578", LocalDate.now());
		gestor.getListaFacturas().add(factura);
		gestor.getListaClientes().add(cliente);
		gestor.asignarClienteAFactura(cliente.getDni(), factura.getCodigoFactura());
		Cliente actual = gestor.buscarFactura(factura.getCodigoFactura()).getCliente();
		Cliente esperado = cliente;
		assertEquals(esperado, actual);
	}
	//gestor no asigna un cliente que no tiene almacenado a una factura
	@Test
	void testAsignarClienteAFactura_FacturaExiste_ClienteNoExiste() {
		Cliente cliente = new Cliente("pepe", "12346578", LocalDate.now());
		gestor.getListaFacturas().add(factura);
		gestor.getListaClientes().add(cliente);
		gestor.asignarClienteAFactura("999666333", factura.getCodigoFactura());
		Cliente actual = gestor.buscarFactura(factura.getCodigoFactura()).getCliente();
		Cliente esperado = null;
		assertEquals(esperado, actual);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
