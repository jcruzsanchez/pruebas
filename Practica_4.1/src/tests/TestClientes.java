package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	GestorContabilidad gestor;
	Cliente cliente;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		gestor = new GestorContabilidad();
		cliente = new Cliente("pepe", "12346578", LocalDate.now());
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	//gestor devuelve el cliente buscado
	@Test
	void testBuscarClienteQueSiExiste() {		
		gestor.getListaClientes().add(cliente);
		Cliente actual = gestor.buscarCliente(cliente.getDni());
		assertEquals(cliente, actual);		
	}
	//gestor devuelve null cuando el cliente que se busca no existe
	@Test
	void testBuscarClienteQueNoExiste() {
		Cliente actual = gestor.buscarCliente(cliente.getDni());
		assertEquals(null, actual);		
	}
	//gestor no contiene un cliente que se ha intentado introducir que ya estaba
	@Test
	void testAltaClienteQueSiExiste() {
		Cliente cliente1 = cliente;
		gestor.getListaClientes().add(cliente);
		gestor.altaCliente(cliente1);
		assertFalse(gestor.getListaClientes().contains(cliente1));
	}
	//gestor almacena el cliente que se intenta introducir
	@Test
	void testAltaClienteQueNoExiste() {
		gestor.altaCliente(cliente);
		assertTrue(gestor.getListaClientes().contains(cliente));
	}
	//Devuelve el cliente mas antiguo
	@Test
	void testClienteMasAntiguoArrayContieneClientes() {
		Cliente cliente1 = new Cliente("roas", "98764531", LocalDate.parse("2015-05-05"));
		Cliente cliente2 = new Cliente("xxxx", "11223344", LocalDate.parse("2014-05-05"));
		gestor.getListaClientes().add(cliente);
		gestor.getListaClientes().add(cliente1);
		gestor.getListaClientes().add(cliente2);
		Cliente actual = cliente;
		Cliente esperado = gestor.clienteMasAntiguo();
		assertSame(esperado, actual);
	}
	//gestor devuelve null si no existen clientes
	@Test
	void testClienteMasAntiguoArrayVacio() {
		Cliente esperado = null;
		Cliente actual = gestor.clienteMasAntiguo();
		assertSame(esperado, actual);
	}
	//el gestor no elimina un cliente si esta enlazado a una factura
	@Test
	void testEliminarClienteQueSiExisteEnListaFactura() {	
		Factura factura = new Factura("xx", LocalDate.now(), "peces", 3, 2);
		factura.setCliente(cliente);
		gestor.getListaClientes().add(cliente);
		gestor.getListaFacturas().add(factura);
		gestor.eliminarCliente(cliente.getDni());
		assertTrue(gestor.getListaClientes().contains(cliente));
	}
	//el gestor elimina el cliente si no esta enlazado a una factura
	@Test
	void testEliminarClienteQueNoExisteEnListaFactura() {
		gestor.getListaClientes().add(cliente);
		gestor.eliminarCliente(cliente.getDni());
		assertFalse(gestor.getListaClientes().contains(cliente));
	}
	//Cuenta cuantas facturas hay de un cliente que si esta enlazado a facturas
	@Test
	void testCantidadFacturasPorClienteArrayFacturasSiContieneEseCliente() {
		Factura factura = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura1 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura2 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		Factura factura3 = new Factura("sss", LocalDate.now(), "Factura 1", 5, 5);
		factura.setCliente(cliente);
		factura1.setCliente(cliente);
		factura2.setCliente(cliente);
		factura3.setCliente(cliente);
		gestor.getListaFacturas().add(factura);
		gestor.getListaFacturas().add(factura1);
		gestor.getListaFacturas().add(factura2);
		gestor.getListaFacturas().add(factura3);
		int actual = gestor.cantidadFacturasPorCliente(cliente.getDni());
		int esperado = 4;
		assertEquals(esperado, actual);
	}
	//devuelve 0 si no esta es cliente enlazado a ninguna factura
	@Test
	void testCantidadFacturasPorClienteArrayFacturasNoContieneEseCliente() {
		int actual = gestor.cantidadFacturasPorCliente("dni no existe");
		int esperado = 0;
		assertEquals(esperado, actual);
	}
	
	
	
	
	
	
	
	
	
	
	
}
